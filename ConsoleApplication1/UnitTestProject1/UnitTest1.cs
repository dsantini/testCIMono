﻿using System;
using NUnit.Framework;
using ConsoleApplication1;

namespace UnitTestProject1
{
    [TestFixture]
    public class UnitTest1
    {
        [Test]
        public void TestProgramMoltiplicazione1()
        {
            int a = 10, b = 2, c = Program.testMoltiplica(a, b);
            Assert.AreEqual(c, 20);
        }

        [Test]
        public void TestProgramDivisione1()
        {
            int a = 10, b = 2, c = Program.testDividi(a, b);
            Assert.AreEqual(c, 5);
        }
    }
}

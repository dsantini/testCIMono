﻿using System;
using NUnit.Framework;
using ConsoleApplication1;

namespace UnitTestProject2
{
	[TestFixture]
	public class UnitTest2
	{
		[Test]
		public void TestProgramMoltiplicazione2()
		{
			int a = 10, b = 2, c = Program.testMoltiplica(a, b);
			Assert.AreEqual(c, 20);
		}

		[Test]
		public void TestProgramDivisione2()
		{
			int a = 10, b = 2, c = Program.testDividi(a, b);
			Assert.AreEqual(c, 5);
		}
	}
}
